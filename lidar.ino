/*
http://pulsedlight3d.com
 LIDAR-Lite Continuous Pulse-Width Modulation
 The LIDAR-Lite has the ability to read distance without the use of I2C, simply 
 by reading the amount of time a signal from the mode pin is HIGH. We then take that
 duration (here measured in microseconds) and convert it to centimeters using the 
 LIDAR-Lite constant of 10usec/cm. We then print that value to the serial monitor. 
 Technical explanation is on page 15/16 of the operating manual: 
 http://pulsedlight3d.com/pl3d/wp-content/uploads/2014/11/LIDAR-Lite-Operating-Manual.pdf
 */

unsigned long pulse_width1, pulse_width2;

void setup()
{
  Serial.begin(9600); // Start serial communications
  pinMode(2, OUTPUT); // Set pin 2 as trigger pin for ldar 1
  pinMode(4, OUTPUT); // Set pin 4 as trigger pin for ldar 2
  pinMode(3, INPUT); // Set pin 3 as monitor pin for ldar 1
  pinMode(5, INPUT); // Set pin 5 as monitor pin for ldar 2
  pinMode(9, OUTPUT); // Set pin 6 as analog pin for ldar 1
  pinMode(10, OUTPUT); // Set pin 7 as analog pin for ldar 2
  digitalWrite(2, LOW); // Set trigger LOW for continuous read for ldar 1
  digitalWrite(4, LOW); // Set trigger LOW for continuous read for ldar 2
}

void loop()
{
  pulse_width1 = pulseIn(3, HIGH); // Count how long the pulse is high in microseconds for ldar 1
  pulse_width2 = pulseIn(5, HIGH); // Count how long the pulse is high in microseconds for ldar 2
  pulse_width1 = pulse_width1/10; // 10usec = 1 cm of distance for LIDAR-Lite
  pulse_width2 = pulse_width2/10; // 10usec = 1 cm of distance for LIDAR-Lite
  Serial.print(pulse_width1);
  Serial.print(",");
  Serial.println(pulse_width2); // Print the distances
  analogWrite(9, pulse_width1);
  analogWrite(10, pulse_width2);
  delay(20); //Delay so we don't overload the serial port
}

